function formatFullName(defaultObj,obj)
{
    let fullName='';

    for (let key in defaultObj)
    {
        if(defaultObj[key] in obj)
        {
            fullName+=(obj[defaultObj[key]]);
            
            //console.log(fullName);
            fullName+=" ";
        }
    }

    fullName=fullName.toLowerCase();
    fullName=fullName.split(' ');

    for(let key in fullName)
    {
        fullName[key]=fullName[key].charAt(0).toUpperCase() + fullName[key].slice(1);
    }

    fullName=fullName.join(' ');
    return fullName;
}
//let defaultObj={name:"name"};
//let obj={name:"name"};
//formatFullName(defaultObj,obj);
module.exports=formatFullName;