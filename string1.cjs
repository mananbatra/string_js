function convertToNumericFormat(str) {
  const cleanedStr = str.replace(/[^0-9.-]/g, ''); // Remove non-numeric characters except dot and minus sign

  if (cleanedStr === '' || isNaN(cleanedStr)) {
    return 0; // Return 0 for invalid or empty strings
  }

  return parseFloat(cleanedStr); // Convert the cleaned string to a floating-point number
}

//console.log(convertToNumericFormat("ifwieb,241huvudeu314,351235td,$10.2945748"))
module.exports=convertToNumericFormat;