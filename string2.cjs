function splitAddress(ipAddress) {
  if (ipAddress = null) {
    return [];
  }

  const piece = ipAddress.split('.');//console.log(piece);

  if (piece.length !== 4) {
    return []; // Invalid IP address format
  }
  const numBits = [];

  for (let index in piece) {
    numBits[index] = +piece[index];//console.log(numBits)

    if (Number.isNaN(numBits[index])) {
      return []; // Invalid characters detected
    }
  }

  for (let index in numBits) {
    if (numBits[index] > 255 || numBits[index] < 0) {
      return [];
    }
  }


  return numBits;
}

module.exports = splitAddress;

//const ipAddress = "111.139.161.143";
//const result = splitAddress(ipAddress);
//console.log(result); // [111, 139, 161, 143]
