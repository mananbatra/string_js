function convertToString(arr)
 {
    if (arr.length === 0) 
    {
      return ""; // Empty array, return empty string
    }
  
    return arr.join(" ")+".";
  }

  module.exports=convertToString;
  
  //const array = ["the", "quick", "brown", "fox"];
  //const result = convertToString(array);
  //console.log(result); // "the quick brown fox"
  